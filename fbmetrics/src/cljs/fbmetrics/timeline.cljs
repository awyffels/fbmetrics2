(ns fbmetrics.timeline
 
  (:require [ajax.core :as ajax  :refer [GET]]
            [clojure.string :as cstr]))

(defn circ []
  ""
  [:div {:class "tl-circ"}])

(defn date-bar [date]
  ""
  [:li [:div {:class "tldate"} date]])

(defn timerow [headline time]
  ""
   [:div {:class "timeline-panel"}
    [:div {:class "tl-heading"}
     [:h4 headline]
     [:p
      [:small {:class "text-muted"}
       [:i {:class "glyphicon glyphicon-time"}] time]]]])

(defn timeline-panel [headline time]
  ""
  [:li
   (circ)
   (timerow headline time)])

(defn timeline-panel-date [date headline time]
  ""
  [:div (date-bar date)
  [:li
   (circ)
   (timerow headline time)]])

(defn inverted-timeline-panel [headline time]
  ""
  [:li {:class "timeline-inverted"}
   (circ)
   (timerow headline time)])

(defn inverted-timeline-panel-date [date headline time]
  ""
  [:div (date-bar date)
  [:li {:class "timeline-inverted"}
   (circ)
     (timerow headline time)]])

(defn get-month-str [month]
  ""
  (cond
   (= month "1") "January"
   (= month "2") "February"
   (= month "3") "March"
   (= month "4") "April"
   (= month "5") "May"
   (= month "6") "June"
   (= month "7") "July"
   (= month "8") "August"
   (= month "9") "September"
   (= month "10") "October"
   (= month "11") "November"
   (= month "12") "December"
   :else
   "Undefined"))

;; -------------------------
;; Model
(defonce current-timeline (atom 0))

;; -------------------------
;; Actions
(declare timeline)
(defn update-timeline [response]
  (.log js/console "Response =>" response)  
  (reset! current-timeline response))
 ;;(timeline
 ;;            [{:headline "first headline"
 ;;              :date " 1/7/2016 3:00PM"}]))

(defn request-more-timeline []
  (.log js/console "Making request!...")
  (ajax/GET "/next-timeline-section"
            {:handler update-timeline}))
            

(defn timeline [mymap]
  ""
  [:h1 "Timeline"]
                  [:div
                   [:ul {:class "timeline"}
                    (println "MYMAP " mymap)
                    (let [count (atom 0)
                          current-month (atom "")
                          month (atom "")]
                      (for [x mymap]
                        (do                        
                          (println x)
                          (reset! current-month (get-month-str
                                                 (cstr/trim
                                                  (first (cstr/split (get x :date) #"/")))))
                          (println "Current Month " @current-month)
                          (println "Month " @month)
                          (println (= @current-month @month))
                          
                          [:div
                          (if (even? @count)                           
                            (do
                              (swap! count inc)
                              (if (= @current-month @month)
                                (do
                                  (println 1)
                                  (reset! month @current-month)
                                  (timeline-panel (get x :headline) (get x :date)))
                                (do
                                  (println 2)
                                (reset! month @current-month)
                                (timeline-panel-date @current-month
                                                     (get x :headline) (get x :date)))))
                            (do
                              (swap! count inc)
                              (if (= @current-month @month)
                                (do
                                  (println 3)
                                  (reset! month @current-month)
                                  (inverted-timeline-panel (get x :headline) (get x :date)))
                                (do
                                  (println 4)
                                  (reset! month @current-month)
                                  (inverted-timeline-panel-date @current-month (get x :headline)
                                                                (get x :date))))))])))
                    [:div
                     [:li
                      [:div {:class "tl-more-btn"}
                       [:button {:class "btn btn-primary" :on-click request-more-timeline} (str @current-timeline)]]]]
                    ]])
