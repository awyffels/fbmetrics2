(ns fbmetrics.core
    (:require [reagent.core :as reagent :refer [atom]]
              [reagent.session :as session]
              [secretary.core :as secretary :include-macros true]
              [accountant.core :as accountant]
              [fbmetrics.timeline :as view]))

(def mymap [{:headline "first headline"
             :date " 1/7/2016 3:00PM"}
            {:headline "second headline"
             :date " 2/10/2016 5:00PM"}
            {:headline "third headline"
             :date " 2/20/2016 1:00AM"}
            {:headline "fourth headline"
             :date " 6/20/2016 1:00AM"}])
;; -------------------------
;; Views

(defn home-page []
  [:div [:h2 "Welcome to fbmetrics"]
   [:div [:a {:href "/about"} "go to about page"]]
   [:div [:a {:href "/timeline"} "go to timeline page"]]])

(defn about-page []
  [:div [:h2 "About fbmetrics"]
   [:div [:a {:href "/"} "go to the home page"]]])

(defn timeline-page []
  ;;(view/update-timeline mymap)
  (view/timeline mymap))

(defn current-page []
  [:div [(session/get :current-page)]])

;; -------------------------
;; Routes

(secretary/defroute "/" []
  (session/put! :current-page #'home-page))

(secretary/defroute "/about" []
  (session/put! :current-page #'about-page))

(secretary/defroute "/timeline" []
  (session/put! :current-page #'timeline-page))

;; -------------------------
;; Initialize app

(defn mount-root []
  (reagent/render [current-page] (.getElementById js/document "app")))

(defn init! []
  (accountant/configure-navigation!
    {:nav-handler
     (fn [path]
       (secretary/dispatch! path))
     :path-exists?
     (fn [path]
       (secretary/locate-route path))})
  (accountant/dispatch-current!)
  (mount-root))
