(ns fbmetrics.handler
  (:require [compojure.core :refer [GET defroutes]]
            [compojure.route :refer [not-found resources]]
            [hiccup.page :refer [include-js include-css html5]]
            [fbmetrics.middleware :refer [wrap-middleware]]
            [config.core :refer [env]]))

(def mount-target
  [:div#app
      [:h3 "ClojureScript has not been compiled!"]
      [:p "please run "
       [:b "lein figwheel"]
       " in order to start the compiler"]])

(defn head []
  [:head
   [:meta {:charset "utf-8"}]
   [:meta {:name "viewport"
           :content "width=device-width, initial-scale=1"}]
   (include-css (if (env :dev) "/css/site.css" "/css/site.min.css"))
   (include-css "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css")])

(def loading-page
  (html5
    (head)
    [:body {:class "body-container"}
     mount-target
     (include-js "/js/app.js")
    ;; (include-js "https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js")
     (include-js "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js")]))

(def my-num (atom 0))

(defroutes routes
  (GET "/" [] loading-page)
  (GET "/about" [] loading-page)
  ;; with this removed the link can not be directly accessed
  ;;(GET "/timeline" [] loading-page)

  (GET "/next-timeline-section" [] (str (swap! my-num inc)))
  
  (resources "/")
  (not-found "Not Found"))

(def app (wrap-middleware #'routes))
